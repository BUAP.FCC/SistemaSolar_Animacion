//#include<windows.h>
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <stdlib.h>
#include <iostream>
#include <stdio.h>
#include <math.h>
#include "imageloader.h"
using namespace std;

class astronomicalObject
{
    private:
    GLuint _textureId; //The id of the textur
    GLUquadric *quad;
    Image *image;

    GLfloat tetha;
    GLfloat Px,Py,Pz;
    GLfloat Tx,Ty,Tz;
    GLfloat Sx,Sy,Sz;
    GLfloat Rx,Ry,Rz;
    GLfloat velocidad;
    GLfloat RxAxis,RyAxis,RzAxis;
    GLfloat velocidadAxis;
    GLfloat mayorAxis,minorAxis;
    int cameraDistance;
    public:
    GLfloat materialAmbient  [4];
    GLfloat materialDiffuse  [4];
    GLfloat materialSpecular [4];
    GLfloat materialShininess[1];
    void translation(void);
    void rotation(void);
    void scalation(void);
    void pushMatrix(void);
    void popMatrix(void);

    GLuint get_textureId(void);
    GLUquadric* getQuadric(void);
    Image* getImage(void);

    GLfloat getPx(void);
    GLfloat getPy(void);
    GLfloat getPz(void);
    GLfloat getTx(void);
    GLfloat getTy(void);
    GLfloat getTz(void);
    GLfloat getTetha(void);
    GLfloat getVelocidad(void);
    GLfloat getMayorAxis(void);
    GLfloat getMinorAxis(void);

    void set_textureId(GLuint _textureId);
    void setQuadric(GLUquadric *quad);
    void setImage(Image *image);
    void setCameraDistance(int cameraDistance);

    void setTx(GLfloat Tx);
    void setTy(GLfloat Ty);
    void setTz(GLfloat Tz);
    void setRx(GLfloat Rx);
    void setRy(GLfloat Ry);
    void setRz(GLfloat Rz);
    void setSx(GLfloat Sx);
    void setSy(GLfloat Sy);
    void setSz(GLfloat Sz);
    void setRxAxis(GLfloat RxAxis);
    void setRyAxis(GLfloat RyAxis);
    void setRzAxis(GLfloat RzAxis);
    void setVelocidad(GLfloat velocidad);
    void setVelocidadAxis(GLfloat velocidadAxis);
    void setMayorAxis(GLfloat mayorAxis);
    void setMinorAxis(GLfloat minorAxis);
    void camera(GLfloat zoom,int typeCamera);
    double fromDegreesToRads(GLfloat gamma);
    astronomicalObject();

    GLuint loadTexture(Image* image);
};

class Star : public astronomicalObject
{
    private:
    GLdouble radius;
    GLint slices, stacks;
    GLfloat lightAmbient     [4];
    GLfloat lightDiffuse     [4];
    GLfloat lightSpecular    [4];
    GLfloat lightPosition    [4];
    GLfloat materialEmission [4];
    public:
    Star();
    void draw(void);
    void initLight(GLfloat xPos, GLfloat yPos, GLfloat zPos);
    void initTextures(void);
};
class Moon : public astronomicalObject
{
    private:
    GLfloat R,G,B;
    GLdouble radius;
    GLint slices, stacks;
    public:
    Moon();
    void create(GLdouble radius,GLint slices, GLint stacks, GLfloat minorAxis, GLfloat mayorAxis, GLfloat velocidad);
    void draw(void);
};

class Mercury : public astronomicalObject
{
    private:
    GLfloat R,G,B;
    GLdouble radius;
    GLint slices, stacks;
    public:
    Mercury();
    void draw(void);
    void initTextures(void);
};
class Venus : public astronomicalObject
{
    private:
    GLfloat R,G,B;
    GLdouble radius;
    GLint slices, stacks;
    public:
    Venus();
    void draw(void);
    void initTextures(void);
};
class Earth : public astronomicalObject
{
    private:
    GLfloat R,G,B;
    GLdouble radius;
    GLint slices, stacks;
    Moon earthMoon;
    public:
    Earth();
    void draw(void);
    void initTextures(void);
};
class Mars : public astronomicalObject
{
    private:
    GLfloat R,G,B;
    GLdouble radius;
    GLint slices, stacks;
    Moon phobos,deimos;
    public:
    Mars();
    void draw(void);
    void initTextures(void);
};
class Jupiter : public astronomicalObject
{
    private:
    GLfloat R,G,B;
    GLdouble radius;
    GLint slices, stacks;
    Moon europa,ganymede,callisto,io;
    public:
    Jupiter();
    void draw(void);
    void initTextures(void);
};
class Saturn : public astronomicalObject
{
    private:
    GLdouble radius;
    GLint slices, stacks;

    GLUquadric *quadricRings;
    GLuint textureRings;
    Image* textureRingsImage;

    Moon mimas,enceladus,tethys,dione,rhea,titan,iapetus;

    public:
    Saturn();
    void draw(void);
    GLUquadric* getRingsQuad(void);
    GLuint getRingsTexture(void);
    Image* getRingsTextureImage(void);
    void setRingsQuad(GLUquadric* quadricRings);
    void setRingsTexture(GLuint textureRings);
    void setRingsTextureImage(Image* textureRingsImage);
    void initTextures(void);
};
class Uranus : public astronomicalObject
{
    private:
    GLfloat R,G,B;
    GLdouble radius;
    GLint slices, stacks;
    Moon puck,miranda,ariel,umbriel,titania,oberon;
    public:
    Uranus();
    void draw(void);
    void initTextures(void);
};
class Neptune : public astronomicalObject
{
    private:
    GLfloat R,G,B;
    GLdouble radius;
    GLint slices, stacks;
    Moon triton;
    public:
    Neptune();
    void draw(void);
    void initTextures(void);
};

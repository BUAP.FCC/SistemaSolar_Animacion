#include "libreriaEntorno.h"
void init(void);
void display(void);
void reshape(int w, int h);
void keyPressed(unsigned char key, int x ,int y);
void showPlanet(int option);
GLuint loadTexture(Image* image);
static void idle(void);

Star sun;
Mercury mercury;
Venus venus;
Earth earth;
Mars mars;
Jupiter jupiter;
Saturn saturn;
Uranus uranus;
Neptune neptune;
/************************************************/
GLfloat zoom = 0.0;
int optionPlanet = 0;
int optionCamera = 0;
/************************************************/

int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(1200,1000);
    glutInitWindowPosition(0,0);
    glutCreateWindow("Sistema Solar");
    cout<<" ------------------------------------------------"<<endl;
    cout<<"                                                 "<<endl<<endl;
    cout<<"      Espera un momento... cargando texturas     "<<endl;
    init();
    cout<<"      ";system("pause"); system("cls");
    cout<<" ------------------------------------------------"<<endl;
    cout<<" Instrucciones: Presiona teclas numericas y elije"<<endl<<endl;
    cout<<" (1) Mercurio  (2) Venus    (3) Tierra  (4) Marte  "<<endl;
    cout<<" (5) Jupiter   (6) Saturno  (7) Urano   (8) Neptuno"<<endl<<endl;
    cout<<" Control de zoom    (I) Acercar     (O) Alejar     "<<endl;
    cout<<" Camaras            (R) Rotatoria   (E) Encima    "<<endl;
    system("PAUSE");
    //PlaySound("interestellar.wav",NULL,SND_ASYNC|SND_FILENAME|SND_LOOP);
    glutKeyboardFunc(keyPressed);
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutIdleFunc(idle);
    glutMainLoop();

    return 0;
}
void keyPressed(unsigned char key, int x ,int y)
{
       switch (key)
      {
           case 'i':
               if(zoom>0)
                zoom-=20;
           break;
           case 'o':
               if(zoom<2500)
                zoom+=20;
           break;
           case '1':
               optionPlanet = 1;
           break;
           case '2':
               optionPlanet = 2;
           break;
           case '3':
               optionPlanet = 3;
           break;
           case '4':
               optionPlanet = 4;
           break;
           case '5':
               optionPlanet = 5;
           break;
           case '6':
               optionPlanet = 6;
           break;
           case '7':
               optionPlanet = 7;
           break;
           case '8':
               optionPlanet = 8;
           break;
           case 'r': case 'R':
               optionCamera = 0;
           break;
           case 'e': case 'E':
               optionCamera = 1;
           break;
      }

}
void showPlanet(int option)
{
    switch(option)
    {
        case 1:
            mercury.camera(zoom,optionCamera);
        break;
        case 2:
            venus.camera(zoom,optionCamera);
        break;
        case 3:
            earth.camera(zoom,optionCamera);
        break;
        case 4:
            mars.camera(zoom,optionCamera);
        break;
        case 5:
            jupiter.camera(zoom,optionCamera);
        break;
        case 6:
            saturn.camera(zoom,optionCamera);
        break;
        case 7:
            uranus.camera(zoom,optionCamera);
        break;
        case 8:
            neptune.camera(zoom,optionCamera);
        break;
    }
}
void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    showPlanet(optionPlanet);
        sun.pushMatrix();
            sun.initLight(0,0,0);
            sun.draw();
        sun.popMatrix();
        mercury.pushMatrix();
            mercury.translation();
            mercury.rotation();
            mercury.scalation();
            mercury.draw();
        mercury.popMatrix();
        venus.pushMatrix();
            venus.translation();
            venus.rotation();
            venus.scalation();
            venus.draw();
        venus.popMatrix();
        earth.pushMatrix();
            earth.translation();
            earth.rotation();
            earth.scalation();
            earth.draw();
        earth.popMatrix();
        mars.pushMatrix();
            mars.translation();
            mars.rotation();
            mars.scalation();
            mars.draw();
        mars.popMatrix();
        jupiter.pushMatrix();
            jupiter.translation();
            jupiter.rotation();
            jupiter.scalation();
            jupiter.draw();
        jupiter.popMatrix();
        saturn.pushMatrix();
            saturn.translation();
            saturn.rotation();
            saturn.scalation();
            saturn.draw();
        saturn.popMatrix();
        uranus.pushMatrix();
            uranus.translation();
            uranus.rotation();
            uranus.scalation();
            uranus.draw();
        uranus.popMatrix();
        neptune.pushMatrix();
            neptune.translation();
            neptune.rotation();
            neptune.scalation();
            neptune.draw();
        neptune.popMatrix();
    glutSwapBuffers();
}
void init(void)
{
    glClearColor(0.0,0.0,0.0,0.0);
    glShadeModel(GL_SMOOTH);

    glEnable(GL_DEPTH_TEST);
    glClearDepth(1.0f);
    glDepthFunc(GL_LEQUAL);

    glEnable(GL_NORMALIZE);
    glEnable(GL_LIGHTING);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glEnable(GL_TEXTURE_2D);

    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    /************************************ INIT TEXTURES **************************************/
    sun.initTextures();
    mercury.initTextures();
    venus.initTextures();
    earth.initTextures();
    mars.initTextures();
    jupiter.initTextures();
    saturn.initTextures();
    uranus.initTextures();
    neptune.initTextures();
}
int j=0;
void reshape(int w, int h)
{
    glViewport(0, 0, (GLsizei)w, (GLsizei)h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0,(GLfloat)w/(GLfloat)h,1.0,3000.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}
static void idle(void)
{
    glutPostRedisplay();
}
//Makes the image into a texture, and returns the id of the texture
GLuint loadTexture(Image* image) {
	GLuint textureId;
	glGenTextures(1, &textureId); //Make room for our texture
	glBindTexture(GL_TEXTURE_2D, textureId); //Tell OpenGL which texture to edit
	//Map the image to the texture
	glTexImage2D(GL_TEXTURE_2D,                //Always GL_TEXTURE_2D
				 0,                            //0 for now
				 GL_RGB,                       //Format OpenGL uses for image
				 image->width, image->height,  //Width and height
				 0,                            //The border of the image
				 GL_RGB, //GL_RGB, because pixels are stored in RGB format
				 GL_UNSIGNED_BYTE, //GL_UNSIGNED_BYTE, because pixels are stored
				                   //as unsigned numbers
				 image->pixels);               //The actual pixel data
	return textureId; //Returns the id of the texture
}



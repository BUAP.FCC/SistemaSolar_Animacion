#include "libreriaEntorno.h"
astronomicalObject::astronomicalObject()
{
    tetha   = 0;
    Px = Py = Pz = 0.0;
    quad = gluNewQuadric();
    cameraDistance = 10;

    materialAmbient  [0] = 0.0; materialAmbient  [1] = 0.0; materialAmbient  [2] = 0.0; materialAmbient  [3] = 1.0;
    materialDiffuse  [0] = 1.0; materialDiffuse  [1] = 1.0; materialDiffuse  [2] = 1.0; materialDiffuse  [3] = 1.0;
    materialSpecular [0] = 0.5; materialSpecular [1] = 0.5; materialSpecular [2] = 0.5; materialSpecular [3] = 1.0;
    materialShininess[0] = 100;
}
void astronomicalObject::translation(void)
{
    glRotatef(tetha*velocidad,Rx,Ry,Rz);
    Tx = minorAxis * cos(-fromDegreesToRads(tetha*velocidad));
    Ty = 0;
    Tz = mayorAxis * sin(-fromDegreesToRads(tetha*velocidad));
    glTranslatef(Tx,Ty,Tz);
    tetha+=0.001;
}
void astronomicalObject::rotation(void)
{
    glRotatef(tetha*velocidadAxis,RxAxis,RyAxis,RzAxis);
}
void astronomicalObject::scalation(void)
{
    glScalef(Sx,Sy,Sz);
}
void astronomicalObject::pushMatrix(void)
{
    glPushMatrix();
}
void astronomicalObject::popMatrix(void)
{
    glPopMatrix();
}
GLuint astronomicalObject::get_textureId(void)
{
    return _textureId;
}
GLUquadric* astronomicalObject::getQuadric(void)
{
    return quad;
}
Image* astronomicalObject::getImage(void)
{
    return image;
}
GLfloat astronomicalObject::getPx(void)
{
    return Px;
}
GLfloat astronomicalObject::getPy(void)
{
    return Py;
}
GLfloat astronomicalObject::getPz(void)
{
    return Pz;
}
GLfloat astronomicalObject::getTx(void)
{
    return Tx;
}
GLfloat astronomicalObject::getTy(void)
{
    return Ty;
}
GLfloat astronomicalObject::getTz(void)
{
    return Tz;
}
GLfloat astronomicalObject::getTetha(void)
{
    return tetha;
}
GLfloat astronomicalObject::getVelocidad(void)
{
    return velocidad;
}
GLfloat astronomicalObject::getMayorAxis(void)
{
    return mayorAxis;
}
GLfloat astronomicalObject::getMinorAxis(void)
{
    return minorAxis;
}
void astronomicalObject::set_textureId(GLuint _textureId)
{
    this -> _textureId = _textureId;
}
void astronomicalObject::setQuadric(GLUquadric* quad)
{
    this -> quad = quad;
}
void astronomicalObject::setImage(Image *image)
{
    this -> image = image;
}
void astronomicalObject::setTx(GLfloat Tx)
{
    this -> Tx = Tx;
}
void astronomicalObject::setTy(GLfloat Ty)
{
    this -> Ty = Ty;
}
void astronomicalObject::setTz(GLfloat Tz)
{
    this -> Tz = Tz;
}
void astronomicalObject::setRx(GLfloat Rx)
{
    this -> Rx = Rx;
}
void astronomicalObject::setRy(GLfloat Ry)
{
    this -> Ry = Ry;
}
void astronomicalObject::setRz(GLfloat Rz)
{
    this -> Rz = Rz;
}
void astronomicalObject::setVelocidad(GLfloat velocidad)
{
    this -> velocidad = velocidad;
}
void astronomicalObject::setSx(GLfloat Sx)
{
    this -> Sx = Sx;
}
void astronomicalObject::setSy(GLfloat Sy)
{
    this -> Sy = Sy;
}
void astronomicalObject::setSz(GLfloat Sz)
{
    this -> Sz = Sz;
}
void astronomicalObject::setRxAxis(GLfloat RxAxis)
{
    this -> RxAxis = RxAxis;
}
void astronomicalObject::setRyAxis(GLfloat RyAxis)
{
    this -> RyAxis = RyAxis;
}
void astronomicalObject::setRzAxis(GLfloat RzAxis)
{
    this -> RzAxis = RzAxis;
}
void astronomicalObject::setVelocidadAxis(GLfloat velocidadAxis)
{
    this -> velocidadAxis = velocidadAxis;
}
void astronomicalObject::setMayorAxis(GLfloat mayorAxis)
{
    this -> mayorAxis = mayorAxis;
}
void astronomicalObject::setMinorAxis(GLfloat minorAxis)
{
    this -> minorAxis = minorAxis;
}
void astronomicalObject::setCameraDistance(int cameraDistance)
{
    this -> cameraDistance = cameraDistance;
}
void astronomicalObject:: camera(GLfloat zoom, int typeCamera)
{
    GLfloat dVector = sqrt(pow(Tx,2)+ pow(Tz,2));
    GLfloat deltaAxis = mayorAxis - minorAxis;
    GLfloat lookX, lookY, lookZ;
    int xUp, yUp, zUp;
    switch(typeCamera)
    {
        case 0:
            Px    = (dVector * cos(fromDegreesToRads(-tetha*velocidad*2)))+  deltaAxis + cameraDistance;
            Py    = zoom;
            Pz    = dVector * sin(fromDegreesToRads(-tetha*velocidad*2));
            lookX = Px - deltaAxis - cameraDistance;
            lookY = 0;
            lookZ = Pz;
            xUp   = 0;
            yUp   = 1;
            zUp   = 0;
        break;
        case 1:
            Px    =  (dVector * cos(fromDegreesToRads(-tetha*velocidad*2))) ;
            Py    =  50;
            Pz    =  (dVector * sin(fromDegreesToRads(-tetha*velocidad*2))) ;
            lookX = 0;
            lookY = 0;
            lookZ = 0;
            xUp   = 0;
            yUp   = 1;
            zUp   = 0;
        break;
    }
    gluLookAt(Px,Py,Pz,lookX,lookY,lookZ,xUp,yUp,zUp);
}
double astronomicalObject::fromDegreesToRads(GLfloat gamma)
{
    double pii = acos(-1);
    double rotation_angle = (double) gamma / 180.0 * pii;
    return rotation_angle;
}
GLuint astronomicalObject::loadTexture(Image* image) {
	GLuint textureId;
	glGenTextures(1, &textureId); //Make room for our texture
	glBindTexture(GL_TEXTURE_2D, textureId); //Tell OpenGL which texture to edit
	//Map the image to the texture
	glTexImage2D(GL_TEXTURE_2D,                //Always GL_TEXTURE_2D
				 0,                            //0 for now
				 GL_RGB,                       //Format OpenGL uses for image
				 image->width, image->height,  //Width and height
				 0,                            //The border of the image
				 GL_RGB, //GL_RGB, because pixels are stored in RGB format
				 GL_UNSIGNED_BYTE, //GL_UNSIGNED_BYTE, because pixels are stored
				                   //as unsigned numbers
				 image->pixels);               //The actual pixel data
	return textureId;
}

Star::Star()
{
    this -> radius = 200;
    this -> slices = 50;
    this -> stacks = 20;
}
void Star::initLight(GLfloat xPos, GLfloat yPos, GLfloat zPos)
{
    lightAmbient [0] = 0.75;     lightAmbient [1] = 0.75;     lightAmbient [2] = 0.75;     lightAmbient [3] = 0.0;
    lightDiffuse [0] = 1.0;      lightDiffuse [1] = 1.0;      lightDiffuse [2] = 1.0;      lightDiffuse [3] = 0.0;
    lightSpecular[0] = 1.0;      lightSpecular[1] = 1.0;      lightSpecular[2] = 1.0;      lightSpecular[3] = 0.0;
    lightPosition[0] = xPos;     lightPosition[1] = yPos;     lightPosition[2] = zPos;     lightPosition[3] = 1.0;

    glEnable(GL_LIGHT0);

    glLightfv(GL_LIGHT0, GL_AMBIENT , lightAmbient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE , lightDiffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular);
    glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);

    /***********************************************SPOTLIGHT******************************************************/
    glEnable(GL_LIGHT1);
    lightPosition[0] = xPos+350;     lightPosition[1] = yPos;     lightPosition[2] = zPos;     lightPosition[3] = 1.0;
    glLightfv(GL_LIGHT1, GL_POSITION,       lightPosition);
    glLightfv(GL_LIGHT1, GL_AMBIENT , lightAmbient);
    glLightfv(GL_LIGHT1, GL_DIFFUSE , lightDiffuse);
    glLightfv(GL_LIGHT1, GL_SPECULAR, lightSpecular);
    glLightf (GL_LIGHT1, GL_SPOT_CUTOFF,    95.0);
    glLightf (GL_LIGHT1, GL_SPOT_EXPONENT,  0.0);
    glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, lightPosition);

    glEnable(GL_LIGHT2);
    lightPosition[0] = xPos-350;     lightPosition[1] = yPos;     lightPosition[2] = zPos;     lightPosition[3] = 1.0;
    glLightfv(GL_LIGHT2, GL_POSITION,       lightPosition);
    glLightfv(GL_LIGHT2, GL_AMBIENT , lightAmbient);
    glLightfv(GL_LIGHT2, GL_DIFFUSE , lightDiffuse);
    glLightfv(GL_LIGHT2, GL_SPECULAR, lightSpecular);
    glLightf (GL_LIGHT2, GL_SPOT_CUTOFF,    95.0);
    glLightf (GL_LIGHT2, GL_SPOT_EXPONENT,  0.0);
    glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, lightPosition);

    glEnable(GL_LIGHT3);
    glLightfv(GL_LIGHT3, GL_AMBIENT , lightAmbient);
    lightPosition[0] = xPos+100;     lightPosition[1] = yPos;     lightPosition[2] = zPos+350;     lightPosition[3] = 1.0;
    glLightfv(GL_LIGHT3, GL_POSITION,       lightPosition);
    glLightfv(GL_LIGHT3, GL_AMBIENT , lightAmbient);
    glLightfv(GL_LIGHT3, GL_DIFFUSE , lightDiffuse);
    glLightfv(GL_LIGHT3, GL_SPECULAR, lightSpecular);
    glLightf (GL_LIGHT3, GL_SPOT_CUTOFF,    95.0);
    glLightf (GL_LIGHT3, GL_SPOT_EXPONENT,  0.0);
    glLightfv(GL_LIGHT3, GL_SPOT_DIRECTION, lightPosition);;

    glEnable(GL_LIGHT4);
    lightPosition[0] = xPos+100;     lightPosition[1] = yPos;     lightPosition[2] = zPos-350;     lightPosition[3] = 1.0;
    glLightfv(GL_LIGHT4, GL_POSITION,       lightPosition);
    glLightfv(GL_LIGHT4, GL_AMBIENT , lightAmbient);
    glLightfv(GL_LIGHT4, GL_DIFFUSE , lightDiffuse);
    glLightfv(GL_LIGHT4, GL_SPECULAR, lightSpecular);
    glLightf (GL_LIGHT4, GL_SPOT_CUTOFF,    95.0);
    glLightf (GL_LIGHT4, GL_SPOT_EXPONENT,  0.0);
    glLightfv(GL_LIGHT4, GL_SPOT_DIRECTION, lightPosition);

    glEnable(GL_LIGHT5);
    lightPosition[0] = xPos;     lightPosition[1] = yPos+350;     lightPosition[2] = zPos;     lightPosition[3] = 1.0;
    glLightfv(GL_LIGHT5, GL_POSITION,       lightPosition);
    glLightfv(GL_LIGHT5, GL_AMBIENT , lightAmbient);
    glLightfv(GL_LIGHT5, GL_DIFFUSE , lightDiffuse);
    glLightfv(GL_LIGHT5, GL_SPECULAR, lightSpecular);
    glLightf (GL_LIGHT5, GL_SPOT_CUTOFF,    120.0);
    glLightf (GL_LIGHT5, GL_SPOT_EXPONENT,  0.0);
    glLightfv(GL_LIGHT5, GL_SPOT_DIRECTION, lightPosition);
}
void Star::draw(void)
{
    glMaterialfv(GL_FRONT, GL_AMBIENT  , materialAmbient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE  , materialDiffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR , materialSpecular);
    glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glBindTexture(GL_TEXTURE_2D,get_textureId());

	//Bottom
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	GLUquadric * quadric = getQuadric();

	gluQuadricTexture(quadric,1);
	glRotatef(270,1,0,0);
    gluSphere(quadric,radius,slices,stacks);
}
void Star::initTextures(void)
{
	cout<<"Loading Sun texture..."<<endl;
    setImage(loadBMP("Textures/sun.bmp"));
    set_textureId(loadTexture(getImage()));
}
//**************************************************** M O O N ' S ************************************************************/
Moon::Moon()
{
    //Axis from Ellipse Equation ... mayorAxis -> radius > radius <- minorAxis
    setRx(0.0);
    setRy(1.0);
    setRz(0.0);
    setSx(0.5);
    setSy(0.5);
    setSz(0.5);
    setRxAxis(0.0);
    setRyAxis(1.0);
    setRzAxis(0.0);
    setVelocidadAxis(0.5);
}
void Moon::create(GLdouble radius,GLint slices,GLint stacks,GLfloat minorAxis, GLfloat mayorAxis, GLfloat velocidad)
{
    this -> radius = radius;
    this -> slices = slices;
    this -> stacks = stacks;
    setMayorAxis(mayorAxis);
    setMinorAxis(minorAxis);
    setVelocidad(velocidad);
}
void Moon::draw(void)
{
    glMaterialfv(GL_FRONT, GL_AMBIENT  , materialAmbient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE  , materialDiffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR , materialSpecular);
    glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glBindTexture(GL_TEXTURE_2D,get_textureId());

	//Bottom
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	gluQuadricTexture(getQuadric(),1);
	glRotatef(270,1,0,0);
    gluSphere(getQuadric(),radius,slices,stacks);
}
//**************************************************** M E R C U R Y ************************************************************/
Mercury::Mercury()
{
    this -> radius = 5;
    this -> slices = 50;
    this -> stacks = 20;
    //Axis from Ellipse Equation ... mayorAxis -> radius > radius <- minorAxis
    setMayorAxis(480.0);
    setMinorAxis(470.0);
    setVelocidad(47.4); //Orbital velocity Km/s
    setRx(0.0);
    setRy(1.0);
    setRz(0.0);
    setSx(0.75);
    setSy(0.75);
    setSz(0.75);
    setRxAxis(0.0);
    setRyAxis(1.0);
    setRzAxis(0.0);
    setVelocidadAxis(100);
}
void Mercury::draw(void)
{
    glMaterialfv(GL_FRONT, GL_AMBIENT  , materialAmbient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE  , materialDiffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR , materialSpecular);
    glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glBindTexture(GL_TEXTURE_2D,get_textureId());

	//Bottom
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	gluQuadricTexture(getQuadric(),1);
	glRotatef(270,1,0,0);
    gluSphere(getQuadric(),radius,slices,stacks);
}
void Mercury::initTextures(void)
{
	cout<<"Loading Mercury texture..."<<endl;
    setImage(loadBMP("Textures/mercury.bmp"));
    set_textureId(loadTexture(getImage()));
}
//**************************************************** V E N U S ************************************************************/
Venus::Venus()
{
    this -> radius = 10;
    this -> slices = 50;
    this -> stacks = 20;
    //Axis from Ellipse Equation ... mayorAxis -> radius > radius <- minorAxis
    setMayorAxis(580.0);
    setMinorAxis(570.0);
    setVelocidad(35.0); //Orbital velocity Km/s
    setRx(0.0);
    setRy(1.0);
    setRz(0.0);
    setSx(0.75);
    setSy(0.75);
    setSz(0.75);
    setRxAxis(0.0);
    setRyAxis(1.0);
    setRzAxis(0.0);
    setVelocidadAxis(100);
}
void Venus::draw(void)
{
    glMaterialfv(GL_FRONT, GL_AMBIENT  , materialAmbient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE  , materialDiffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR , materialSpecular);
    glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glBindTexture(GL_TEXTURE_2D,get_textureId());

	//Bottom
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	gluQuadricTexture(getQuadric(),1);
	glRotatef(270,1,0,0);
    gluSphere(getQuadric(),radius,slices,stacks);
}
void Venus::initTextures(void)
{
	cout<<"Loading Venus texture..."<<endl;
    setImage(loadBMP("Textures/venus.bmp"));
    set_textureId(loadTexture(getImage()));
}
//**************************************************** E A R T H ************************************************************/
Earth::Earth()
{
    this -> radius = 13;
    this -> slices = 50;
    this -> stacks = 20;
    //Axis from Ellipse Equation ... mayorAxis -> radius > radius <- minorAxis
    setMayorAxis(700.0);
    setMinorAxis(680.0);
    setVelocidad(29.8); //Orbital velocity Km/s
    setRx(0.0);     setRy(1.0);     setRz(0.0);
    setSx(0.75);     setSy(0.75);     setSz(0.75);
    setRxAxis(0.0); setRyAxis(1.0); setRzAxis(0.0);
    setVelocidadAxis(100);

    earthMoon.create(3,50,20,40.0,40.0,50);
}
void Earth::draw(void)
{
    glMaterialfv(GL_FRONT, GL_AMBIENT  , materialAmbient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE  , materialDiffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR , materialSpecular);
    glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glBindTexture(GL_TEXTURE_2D,get_textureId());
	//Bottom
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    gluQuadricTexture(getQuadric(),1);

	glRotatef(270,1,0,0);
    gluSphere(getQuadric(),radius,slices,stacks);

    glRotatef(-270,1,0,0);
    earthMoon.pushMatrix();
        earthMoon.translation();
        earthMoon.draw();
    earthMoon.popMatrix();
}
void Earth::initTextures(void)
{
	cout<<"Loading Earth texture..."<<endl;
    setImage(loadBMP("Textures/earth.bmp"));
    set_textureId(loadTexture(getImage()));
    cout<<"...loading Moon texture..."<<endl;
    earthMoon.setImage(loadBMP("Textures/moon.bmp"));
    earthMoon.set_textureId(earthMoon.loadTexture(earthMoon.getImage()));
}
//**************************************************** M A R S  ************************************************************/
Mars::Mars()
{
    this -> radius = 13;
    this -> slices = 50;
    this -> stacks = 20;
    //Axis from Ellipse Equation ... mayorAxis -> radius > radius <- minorAxis
    setMayorAxis(850.0);
    setMinorAxis(840.0);
    setVelocidad(24.1); //Orbital velocity Km/s
    setRx(0.0);     setRy(1.0);     setRz(0.0);
    setSx(0.75);     setSy(0.75);     setSz(0.75);
    setRxAxis(0.0); setRyAxis(1.0); setRzAxis(0.0);
    setVelocidadAxis(100);
    phobos.create(0.2,8,12,15.0,15.0,35);
    deimos.create(0.1,5,8,16.0,16.5,40);
}
void Mars::draw(void)
{
    glMaterialfv(GL_FRONT, GL_AMBIENT  , materialAmbient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE  , materialDiffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR , materialSpecular);
    glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glBindTexture(GL_TEXTURE_2D,get_textureId());
	//Bottom
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    gluQuadricTexture(getQuadric(),1);

	glRotatef(270,1,0,0);
    gluSphere(getQuadric(),radius,slices,stacks);
    glRotatef(-270,1,0,0);

    phobos.pushMatrix();
        phobos.translation();
        phobos.draw();
    phobos.popMatrix();
    deimos.pushMatrix();
        deimos.translation();
        deimos.draw();
    deimos.popMatrix();
}
void Mars::initTextures(void)
{
	cout<<"Loading Mars texture..."<<endl;
    setImage(loadBMP("Textures/mars.bmp"));
    set_textureId(loadTexture(getImage()));

    cout<<"...loading Phobos texture..."<<endl;
    phobos.setImage(loadBMP("Textures/phobosMoon.bmp"));
    phobos.set_textureId(phobos.loadTexture(phobos.getImage()));

    cout<<"...loading Deimos texture..."<<endl;
    deimos.setImage(loadBMP("Textures/deimosMoon.bmp"));
    deimos.set_textureId(deimos.loadTexture(deimos.getImage()));
}
//**************************************************** J U P I T E R ************************************************************/
Jupiter::Jupiter()
{
    this -> radius = 50;
    this -> slices = 50;
    this -> stacks = 20;
    //Axis from Ellipse Equation ... mayorAxis -> radius > radius <- minorAxis
    setMayorAxis(1200.0);
    setMinorAxis(1100.0);
    setVelocidad(13.1); //Orbital velocity Km/s
    setRx(0.0);     setRy(1.0);     setRz(0.0);
    setSx(0.75);     setSy(0.75);     setSz(0.75);
    setRxAxis(0.0); setRyAxis(1.0); setRzAxis(0.0);
    setVelocidadAxis(100);

    io.create(2.8,50,20,63,65,38);
    europa.create(2.3,50,20,70,70,29);
    ganymede.create(4,50,20,100,100,32);
    callisto.create(3.6,50,20,150,150,22.5);
}
void Jupiter::draw(void)
{
    glMaterialfv(GL_FRONT, GL_AMBIENT  , materialAmbient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE  , materialDiffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR , materialSpecular);
    glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glBindTexture(GL_TEXTURE_2D,get_textureId());
	//Bottom
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    gluQuadricTexture(getQuadric(),1);

	glRotatef(270,1,0,0);
    gluSphere(getQuadric(),radius,slices,stacks);
    glRotatef(-240,1,0,0);

    io.pushMatrix();
        io.translation();
        io.draw();
    io.popMatrix();

    europa.pushMatrix();
        europa.translation();
        europa.draw();
    europa.popMatrix();

    ganymede.pushMatrix();
        ganymede.translation();
        ganymede.draw();
    ganymede.popMatrix();

    callisto.pushMatrix();
        callisto.translation();
        callisto.draw();
    callisto.popMatrix();
}
void Jupiter::initTextures(void)
{
	cout<<"Loading Jupiter texture..."<<endl;
    setImage(loadBMP("Textures/jupiter.bmp"));
    set_textureId(loadTexture(getImage()));

    cout<<"...loading Europa texture..."<<endl;
    europa.setImage(loadBMP("Textures/europaMoon.bmp"));
    europa.set_textureId(europa.loadTexture(europa.getImage()));

    cout<<"...loading Ganymede texture..."<<endl;
    ganymede.setImage(loadBMP("Textures/ganymedeMoon.bmp"));
    ganymede.set_textureId(ganymede.loadTexture(ganymede.getImage()));

    cout<<"...loading Callisto texture..."<<endl;
    callisto.setImage(loadBMP("Textures/callistoMoon.bmp"));
    callisto.set_textureId(callisto.loadTexture(callisto.getImage()));

    cout<<"...loading Io texture..."<<endl;
    io.setImage(loadBMP("Textures/ioMoon.bmp"));
    io.set_textureId(io.loadTexture(io.getImage()));
}
//**************************************************** S A T U R N ************************************************************/
Saturn::Saturn()
{
    this -> radius = 45;
    this -> slices = 50;
    this -> stacks = 20;
    //Axis from Ellipse Equation ... mayorAxis -> radius > radius <- minorAxis
    setMayorAxis(1800.0);
    setMinorAxis(1700.0);
    setVelocidad(9.6); //Orbital velocity Km/s
    setRx(0.0);     setRy(1.0);     setRz(0.0);
    setSx(0.75);     setSy(0.75);     setSz(0.75);
    setRxAxis(0.0); setRyAxis(1.0); setRzAxis(0.0);
    setVelocidadAxis(100);
    quadricRings = gluNewQuadric();

    mimas.create(0.5,50,20,70,70,50);
    enceladus.create(0.8,50,20,80,80,30);
    tethys.create(1.5,50,20,90,90,20);
    dione.create(1.8,50,20,110,110,18);
    rhea.create(2.1,50,20,120,120,15);
    titan.create(4,50,20,150,150,25);
    iapetus.create(3,50,20,170,170,22);
}
void Saturn::draw(void)
{
    glMaterialfv(GL_FRONT, GL_AMBIENT  , materialAmbient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE  , materialDiffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR , materialSpecular);
    glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glBindTexture(GL_TEXTURE_2D,get_textureId());
	//Bottom
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    gluQuadricTexture(getQuadric(),1);

        glRotatef(270,1,0,0);
        gluSphere(getQuadric(),radius,slices,stacks);


    glBindTexture(GL_TEXTURE_2D,textureRings);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    gluQuadricTexture(quadricRings,1);
        glRotatef(45,0,1,0);
        pushMatrix();
            gluSphere(quadricRings,120,2,20);
        popMatrix();
        glRotatef(-45,0,1,0);
    glRotatef(-270,1,0,0);
    glRotatef(45,0,0,1);
    glTranslatef(0,3,0);
    mimas.pushMatrix();
            mimas.translation();
            mimas.draw();
    mimas.popMatrix();
    enceladus.pushMatrix();
        enceladus.translation();
        enceladus.draw();
    enceladus.popMatrix();
    tethys.pushMatrix();
        tethys.translation();
        tethys.draw();
    tethys.popMatrix();
    dione.pushMatrix();
        dione.translation();
        dione.draw();
    dione.popMatrix();
    rhea.pushMatrix();
        rhea.translation();
        rhea.draw();
    rhea.popMatrix();
    titan.pushMatrix();
        titan.translation();
        titan.draw();
    titan.popMatrix();
    iapetus.pushMatrix();
        iapetus.translation();
        iapetus.draw();
    iapetus.popMatrix();
}
GLUquadric* Saturn::getRingsQuad(void)
{
    return quadricRings;
}
GLuint Saturn::getRingsTexture(void)
{
    return textureRings;
}
Image* Saturn::getRingsTextureImage(void)
{
    return textureRingsImage;
}
void Saturn::setRingsQuad(GLUquadric * quadricRings)
{
    this -> quadricRings = quadricRings;
}
void Saturn::setRingsTexture(GLuint textureRings)
{
    this -> textureRings = textureRings;
}
void Saturn::setRingsTextureImage(Image* textureRingsImage)
{
    this -> textureRingsImage = textureRingsImage;
}
void Saturn::initTextures(void)
{
    /*   Planet  */
    cout<<"Loading Saturn texture..."<<endl;
    setImage(loadBMP("Textures/saturn.bmp"));
    set_textureId(loadTexture(getImage()));
    cout<<"...loading saturnRings texture..."<<endl;
    setRingsTextureImage(loadBMP("Textures/saturnRings.bmp"));
    setRingsTexture(loadTexture(getRingsTextureImage()));
    /*   Moons   */
    cout<<"...loading Mimas texture..."<<endl;
    mimas.setImage(loadBMP("Textures/mimasMoon.bmp"));
    mimas.set_textureId(mimas.loadTexture(mimas.getImage()));

    cout<<"...loading Enceladus texture..."<<endl;
    enceladus.setImage(loadBMP("Textures/enceladusMoon.bmp"));
    enceladus.set_textureId(enceladus.loadTexture(enceladus.getImage()));

    cout<<"...loading Tethys texture..."<<endl;
    tethys.setImage(loadBMP("Textures/tethysMoon.bmp"));
    tethys.set_textureId(tethys.loadTexture(tethys.getImage()));

    cout<<"...loading Dione texture..."<<endl;
    dione.setImage(loadBMP("Textures/randomMoon1.bmp"));
    dione.set_textureId(dione.loadTexture(dione.getImage()));

    cout<<"...loading Rhea texture..."<<endl;
    rhea.setImage(loadBMP("Textures/randomMoon2.bmp"));
    rhea.set_textureId(rhea.loadTexture(rhea.getImage()));

    cout<<"...loading Titan texture..."<<endl;
    titan.setImage(loadBMP("Textures/titanMoon.bmp"));
    titan.set_textureId(titan.loadTexture(titan.getImage()));

    cout<<"...loading Iapetus texture..."<<endl;
    iapetus.setImage(loadBMP("Textures/iapetusMoon.bmp"));
    iapetus.set_textureId(iapetus.loadTexture(iapetus.getImage()));
}
//**************************************************** U R A N U S ************************************************************/
Uranus::Uranus()
{
    this -> radius = 30;
    this -> slices = 50;
    this -> stacks = 20;
    //Axis from Ellipse Equation ... mayorAxis -> radius > radius <- minorAxis
    setMayorAxis(2500.0);
    setMinorAxis(2450.0);
    setVelocidad(6.8);  //Orbital velocity Km/s
    setRx(0.0);     setRy(1.0);     setRz(0.0);
    setSx(0.75);     setSy(0.75);     setSz(0.75);
    setRxAxis(0.0); setRyAxis(1.0); setRzAxis(0.0);
    setVelocidadAxis(100);

    puck.create(0.3,50,20,33,33.3,18);
    miranda.create(1,50,20,40,44,19.5);
    ariel.create(2.5,50,20,55,59.5,25);
    umbriel.create(2.5,50,20,70,70,12);
    titania.create(3.3,50,20,85,85.2,27);
    oberon.create(3.2,50,20,90,91.5,35);
}
void Uranus::draw(void)
{
    setCameraDistance(1);

    glMaterialfv(GL_FRONT, GL_AMBIENT  , materialAmbient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE  , materialDiffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR , materialSpecular);
    glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glBindTexture(GL_TEXTURE_2D,get_textureId());
	//Bottom
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    gluQuadricTexture(getQuadric(),1);

	glRotatef(270,1,0,0);
    gluSphere(getQuadric(),radius,slices,stacks);
    glRotatef(-270,1,0,0);
    glRotatef(90,0,0,1);

    puck.pushMatrix();
        puck.translation();
        puck.draw();
    puck.popMatrix();
    miranda.pushMatrix();
        miranda.translation();
        miranda.draw();
    miranda.popMatrix();
    ariel.pushMatrix();
        ariel.translation();
        ariel.draw();
    ariel.popMatrix();
    umbriel.pushMatrix();
        umbriel.translation();
        umbriel.draw();
    umbriel.popMatrix();
    titania.pushMatrix();
        titania.translation();
        titania.draw();
    titania.popMatrix();
    oberon.pushMatrix();
        oberon.translation();
        oberon.draw();
    oberon.popMatrix();


}
void Uranus::initTextures(void)
{
	cout<<"Loading Uranus texture..."<<endl;
    setImage(loadBMP("Textures/uranus.bmp"));
    set_textureId(loadTexture(getImage()));

    cout<<"...loading Puck texture..."<<endl;
    puck.setImage(loadBMP("Textures/randomMoon1.bmp"));
    puck.set_textureId(puck.loadTexture(puck.getImage()));

    cout<<"...laoding Miranda texture..."<<endl;
    miranda.setImage(loadBMP("Textures/randomMoon3.bmp"));
    miranda.set_textureId(miranda.loadTexture(miranda.getImage()));

    cout<<"...loading Ariel texture..."<<endl;
    //ariel.setImage(loadBMP("Textures/arielMoon.bmp"));
    ariel.setImage(loadBMP("Textures/randomMoon1.bmp"));
    ariel.set_textureId(ariel.loadTexture(ariel.getImage()));

    cout<<"...loading Umbriel texture..."<<endl;
    umbriel.setImage(loadBMP("Textures/randomMoon2.bmp"));
    umbriel.set_textureId(umbriel.loadTexture(umbriel.getImage()));

    cout<<"...loading Titania texture..."<<endl;
    titania.setImage(loadBMP("Textures/titaniaMoon.bmp"));
    titania.set_textureId(titania.loadTexture(titania.getImage()));

    cout<<"...loading Oberon texture..."<<endl;
    oberon.setImage(loadBMP("Textures/oberonMoon.bmp"));
    oberon.set_textureId(oberon.loadTexture(oberon.getImage()));


}
//**************************************************** N E P T U N E ************************************************************/
Neptune::Neptune()
{
    this -> radius = 30;
    this -> slices = 50;
    this -> stacks = 20;
    //Axis from Ellipse Equation ... mayorAxis -> radius > radius <- minorAxis
    setMayorAxis(2900.0);
    setMinorAxis(2850.0);
    setVelocidad(5.4); //Orbital velocity Km/s
    setRx(0.0);     setRy(1.0);     setRz(0.0);
    setSx(0.75);     setSy(0.75);     setSz(0.75);
    setRxAxis(0.0); setRyAxis(1.0); setRzAxis(0.0);
    setVelocidadAxis(100);

    triton.create(3,50,20,45,50,25);
}
void Neptune::draw(void)
{
    setCameraDistance(1);

    glMaterialfv(GL_FRONT, GL_AMBIENT  , materialAmbient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE  , materialDiffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR , materialSpecular);
    glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glBindTexture(GL_TEXTURE_2D,get_textureId());
	//Bottom
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    gluQuadricTexture(getQuadric(),1);

	glRotatef(270,1,0,0);
    gluSphere(getQuadric(),radius,slices,stacks);
    glRotatef(-270,1,0,0);

    triton.pushMatrix();
        triton.translation();
        triton.draw();
    triton.popMatrix();
}
void Neptune::initTextures(void)
{
	cout<<"Loading Neptune texture..."<<endl;
    setImage(loadBMP("Textures/neptune.bmp"));
    set_textureId(loadTexture(getImage()));

    cout<<"...loading Triton texture...	"<<endl;
    triton.setImage(loadBMP("Textures/tritonMoon.bmp"));
    triton.set_textureId(triton.loadTexture(triton.getImage()));
}
